namespace FoiCoreApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        public Users()
        {
          
        }

        public int Id { get; set; }

        [Required]
        [StringLength(56)]
        public string Username { get; set; }
    }
}
