namespace FoiCoreApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("language")]
    public partial class language
    {
        public int id { get; set; }

        [Required]
        [StringLength(20)]
        public string name { get; set; }
    }
}
