﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoiCoreApi.Models
{
    public class decbylang
    {
        public int id { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.0#####}", ApplyFormatInEditMode = true)]
        public decimal lattitude { get; set; }
        [DisplayFormat(DataFormatString = "{0:0.0#####}", ApplyFormatInEditMode = true)]
        public decimal longitude { get; set; }
        public string img_thumb { get; set; }
        public string img_1 { get; set; }
        public string img_2 { get; set; }
        public string img_3 { get; set; }
        public string name { get; set; }
        public string desc_short { get; set; }
        public int? language { get; set; }

    }
}