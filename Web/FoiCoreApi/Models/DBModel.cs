namespace FoiCoreApi.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBModel : DbContext
    {
        public DBModel()
            : base("name=Online")
        {
        }

        public virtual DbSet<description> description { get; set; }
        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<sight> sight { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<description>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<description>()
                .Property(e => e.desc_short)
                .IsUnicode(false);

            modelBuilder.Entity<sight>()
                .Property(e => e.lattitude)
                .HasPrecision(12, 8);

            modelBuilder.Entity<sight>()
                .Property(e => e.longitude)
                .HasPrecision(12, 8);

            modelBuilder.Entity<sight>()
                .Property(e => e.img_thumb)
                .IsUnicode(false);

            modelBuilder.Entity<sight>()
                .Property(e => e.img_1)
                .IsUnicode(false);

            modelBuilder.Entity<sight>()
                .Property(e => e.img_2)
                .IsUnicode(false);

            modelBuilder.Entity<sight>()
                .Property(e => e.img_3)
                .IsUnicode(false);

        }
    }
}
