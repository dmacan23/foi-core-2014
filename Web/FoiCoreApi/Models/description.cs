namespace FoiCoreApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("description")]
    public partial class description
    {
        public int id { get; set; }

        [Column(TypeName = "text")]
        public string name { get; set; }

        [Column(TypeName = "text")]
        public string desc_short { get; set; }

        public int? language { get; set; }

        public int? sight { get; set; }
    }
}
