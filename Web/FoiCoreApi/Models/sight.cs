namespace FoiCoreApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("sight")]
    public partial class sight
    {
        public int id { get; set; }

        public decimal lattitude { get; set; }

        public decimal longitude { get; set; }

        [Column(TypeName = "text")]
        public string img_thumb { get; set; }

        [Column(TypeName = "text")]
        public string img_1 { get; set; }

        [Column(TypeName = "text")]
        public string img_2 { get; set; }

        [Column(TypeName = "text")]
        public string img_3 { get; set; }
    }
}
