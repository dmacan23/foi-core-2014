﻿function startUp() {

    $(document).foundation();

    $(document).on("click", ".delete", function (e) {

        e.preventDefault();

        $.ajax({

            type: "GET",
            url: $(this).attr("href"),
            success: function (serverData) {

                $(document).ajaxStop(function () {
                    window.location.reload();
                });

            }

        });

    });

    $(document).on("submit", "#edit_form", function (e) {

        e.preventDefault();

        $.ajax({

            type: "POST",
            url: "/Panel/Update/",
            dataType: "json",
            data: $("#edit_form").serialize(),
            success: function (serverData) {

                $('#edit').foundation('reveal', 'close');

                $(document).ajaxStop(function () {
                    window.location.reload();
                });

            }

        });

    })

    $(document).on("submit", "#new_sight", function (e) {

        e.preventDefault();

        $.ajax({

            type: "POST",
            url: "/Panel/Insert",
            data: $("#new_sight").serialize(),
            success: function (serverData) {

                $('#edit').foundation('reveal', 'close');

                $(document).ajaxStop(function () {
                    window.location.reload();
                });

            }

        });

    });
}