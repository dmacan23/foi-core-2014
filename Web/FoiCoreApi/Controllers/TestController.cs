﻿using FoiCoreApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Windows.Forms;

namespace FoiCoreApi.Controllers
{
    public class TestController : ApiController
    {

        DBModel db = new DBModel();

        public object Get()
        {
            //DODAJ POSLJE ASYNC DA SE DOHVACA IZ BAZE DA SE NE CEKA BEZVEZE

            var all = db.sight.ToArray<sight>();

            var description = db.description.ToArray<description>();

            var list = new List<decbylang>();

            foreach (description desc in description)
            {
                foreach (sight sigh in all)
                {
                    if (desc.sight == sigh.id)
                        list.Add(new decbylang
                        {
                            id = sigh.id,
                            lattitude = sigh.lattitude,
                            longitude = sigh.longitude,
                            img_thumb = sigh.img_thumb,
                            img_1 = sigh.img_1,
                            img_2 = sigh.img_2,
                            img_3 = sigh.img_3,
                            name = desc.name,
                            desc_short = desc.desc_short,
                            language = desc.language

                        });
                }
            }

            return list.ToArray();

        }

        public object Post(Poruka model)
        {



            return new Poruka { message = model.status, status = model.message };

        }
    }
}
