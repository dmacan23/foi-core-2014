﻿using FoiCoreApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace FoiCoreApi.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Login(UserViewModel user)
        {
            try
            {

                WebSecurity.Login(user.username,user.password);

                //return JsonConvert.SerializeObject(new { status = true, location = "/Panel/" });

                return RedirectToAction("Index","Panel");
            }
            catch (Exception e)
            {
                //return JsonConvert.SerializeObject(new { status = false, error = "Wrong username and/or password!" });

                return RedirectToAction("Login", "Account");

            }

        }

        public ActionResult Logout()
        {

            WebSecurity.Logout();

            return RedirectToAction("Index","Home");

        }

    }
}
