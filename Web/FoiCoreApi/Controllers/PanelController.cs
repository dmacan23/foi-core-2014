﻿using FoiCoreApi.Classes;
using FoiCoreApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace FoiCoreApi.Controllers
{
    [SimpleAuthorize]
    public class PanelController : Controller
    {
        //
        // GET: /Panel/

        DBModel db = new DBModel();

        public async Task<ActionResult> Index()
        {
            return await Task.Factory.StartNew<ActionResult>(() =>
            {

                IEnumerable<description> descriptions = db.description.ToList<description>();

                return View(descriptions);

            });

        }

        public ActionResult Edit(int id)
        {

            var desc = db.description.Where<description>(x => x.id == id).FirstOrDefault<description>();

            var sight = db.sight.Where<sight>(x => x.id == desc.sight).FirstOrDefault<sight>();

            var allLanguages = from row in db.language select new SelectListItem { Value = row.id.ToString(), Text = row.name };

            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            dictionary.Add("basic", new decbylang
            {
                id = desc.id,
                name = desc.name,
                desc_short = desc.desc_short,
                language = desc.language,
                lattitude = sight.lattitude,
                longitude = sight.longitude,
                img_thumb = sight.img_thumb,
                img_1 = sight.img_1,
                img_2 = sight.img_2,
                img_3 = sight.img_3
            });

            dictionary.Add("languages", allLanguages);

            //System.Windows.Forms.MessageBox.Show(((decbylang)dictionary["basic"]).name);

            return View(dictionary);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Update(decbylang data)
        {
            var desc = db.description.FirstOrDefault<description>(x => x.id == data.id); // u session stavi id poslje da nebi prek hiddena neko mjenjo

            var sig = db.sight.FirstOrDefault<sight>(x => x.id == desc.sight);

            desc.language = data.language;

            desc.name = data.name;

            desc.desc_short = data.desc_short;

            sig.lattitude = data.lattitude;

            sig.longitude = data.longitude;

            sig.img_thumb = data.img_thumb;

            sig.img_1 = data.img_1;

            sig.img_2 = data.img_2;

            sig.img_3 = data.img_3;

            db.SaveChanges();

            return JsonConvert.SerializeObject(new { status = true });

        }

        public string Delete(int id)
        {
            try
            {
                var desc = db.description.FirstOrDefault<description>(x => x.id == id);

                db.sight.Remove(db.sight.FirstOrDefault<sight>(x => x.id == desc.sight));

                //db.SaveChanges();

                db.description.Remove(desc);

                db.SaveChanges();

                return JsonConvert.SerializeObject(new { status = true });
            }
            catch (Exception e)
            {

                return JsonConvert.SerializeObject(new { status = false });

            }

        }

        public ActionResult New()
        {

            var languages = (from row in db.language
                             select new SelectListItem
                             {

                                 Value = row.id.ToString(),
                                 Text = row.name

                             }).ToList<SelectListItem>();

            return View(languages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Insert(decbylang data)
        {
            var id = new sight
            {
                id = 0,
                lattitude = data.lattitude,
                longitude = data.longitude,
                img_thumb = data.img_thumb,
                img_1 = data.img_1,
                img_2 = data.img_2,
                img_3 = data.img_3
            };

            db.sight.Add(id);

            db.SaveChanges();

            db.description.Add(new description
            {
                name = data.name,
                desc_short = data.desc_short,
                language = data.language,
                sight = id.id
            });

            db.SaveChanges();

            return JsonConvert.SerializeObject(data);

        }

    }
}
