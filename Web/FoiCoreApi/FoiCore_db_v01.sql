﻿CREATE TABLE [sight] (
[id] int NOT NULL IDENTITY(1,1),
[lattitude] decimal(12,8) NOT NULL,
[longitude] decimal(12,8) NOT NULL,
[img_thumb] text NULL,
[img_1] text NULL,
[img_2] text NULL,
[img_3] text NULL
);

CREATE TABLE [language] (
[id] int NOT NULL IDENTITY(1,1),
[name] nvarchar(20) NOT NULL
);

CREATE TABLE [description] (
[id] int NOT NULL IDENTITY(1,1),
[name] text NULL,
[desc_short] text NULL,
[desc_long] text NULL,
[language] int NULL,
[sight] int NULL
);


ALTER TABLE [description] ADD FOREIGN KEY ([language]) REFERENCES [language] ([id]);
ALTER TABLE [description] ADD FOREIGN KEY ([sight]) REFERENCES [sight] ([id]);

