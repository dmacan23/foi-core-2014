package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;

/**
 * Created by David on 24.5.2014..
 */
public class Animator {

    private static int lastPosition = -1;

    public static void animateListItem(View item, Context context, int position) {
        animateListItem(item, context, position, R.anim.up_from_bottom, R.anim.down_from_top);
    }

    public static void animateListItem(View item, Context context, int position, int animUp, int animDown) {
        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? animUp : animDown);
        item.startAnimation(animation);
        lastPosition = position;
    }

}
