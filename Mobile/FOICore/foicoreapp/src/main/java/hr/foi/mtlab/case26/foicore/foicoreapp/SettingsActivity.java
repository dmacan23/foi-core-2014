package hr.foi.mtlab.case26.foicore.foicoreapp;

import android.content.res.Configuration;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockPreferenceActivity;

import java.util.Locale;

/**
 * Created by David on 26.5.2014..
 */
public class SettingsActivity extends SherlockPreferenceActivity {

    private Locale locale;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
