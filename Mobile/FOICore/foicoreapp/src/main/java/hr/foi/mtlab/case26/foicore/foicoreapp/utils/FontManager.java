package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by David on 24.5.2014..
 */
public class FontManager {

    public static void setFontToTextView(TextView txt, String fontName) {
        Typeface antonio = Typeface.createFromAsset(txt.getContext()
                .getAssets(), "fonts/" + fontName);
        txt.setTypeface(antonio);
    }

}
