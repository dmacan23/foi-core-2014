package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ahuskano on 5/24/2014.
 */
public class HttpConnection {
    public String readUrl(String mapsApiDirectionsUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            Log.d("internet","1");
            URL url = new URL(mapsApiDirectionsUrl);
            Log.d("internet","2");
            urlConnection = (HttpURLConnection) url.openConnection();
            Log.d("internet","3");
            iStream=null;
            try {
                urlConnection.connect();
            }catch(Exception e){
                return null;
            }
                Log.d("internet","4");
            iStream = urlConnection.getInputStream();
            Log.d("internet","5");
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));
            Log.d("internet","6");
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while reading url", e.toString());
        } finally {
            if(iStream!=null){
                iStream.close();
                urlConnection.disconnect();

            }
          }
        return data;
    }

}