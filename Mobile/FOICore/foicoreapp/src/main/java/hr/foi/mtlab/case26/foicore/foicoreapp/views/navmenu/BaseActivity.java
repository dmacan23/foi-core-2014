package hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu;

import android.content.res.Configuration;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;

/**
 * Created by David on 26.5.2014..
 */
public class BaseActivity extends SlidingFragmentActivity {

    public static int TOUCHMODE_DEFAULT = SlidingMenu.TOUCHMODE_FULLSCREEN;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_name);
        setContentView(R.layout.content_frame);
        setupResponsive();
        setupFragments();
        setupSlidingMenu();
    }

    private void setupResponsive() {
        /**
         * Portrait
         */
        setBehindContentView(R.layout.menu_frame);
        getSlidingMenu().setSlidingEnabled(true);
        TOUCHMODE_DEFAULT = SlidingMenu.TOUCHMODE_FULLSCREEN;
        getSlidingMenu().setTouchModeAbove(TOUCHMODE_DEFAULT);
        getSlidingMenu().setSelectorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /**
         * Landscape
         */
      /*  else {
            View v = new View(this);
            setBehindContentView(v);
            getSlidingMenu().setSlidingEnabled(false);
            TOUCHMODE_DEFAULT = SlidingMenu.TOUCHMODE_NONE;
            getSlidingMenu().setTouchModeAbove(TOUCHMODE_DEFAULT);
        }*/

    }

    private void setupFragments() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new MenuList())
                .commit();

        showFragment(MenuList.menuItems[MenuList.currentItem].getFragment());
    }

    private void showFragment(SherlockFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }


    private void setupSlidingMenu() {
        SlidingMenu slidingMenu = getSlidingMenu();
        slidingMenu.setShadowWidth(10);
        slidingMenu.setShadowDrawable(R.drawable.shadow);
        setupOffset();
        slidingMenu.setFadeDegree(0.35f);
    }

    private void setupOffset() {

        switch (getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                Double offsetL = getWindowManager().getDefaultDisplay().getWidth() * 0.7;
                getSlidingMenu().setBehindOffset(offsetL.intValue());
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                Double offsetP = getWindowManager().getDefaultDisplay().getWidth() * 0.4;
                getSlidingMenu().setBehindOffset(offsetP.intValue());
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Singleton.getInstance().isGoToChosen()) {
            showFragment(MenuList.menuItems[1].getFragment());
            MenuList.currentItem = 1;
            getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            Singleton.getInstance().setGoToChosen(false);
        }
    }


}
