package hr.foi.mtlab.case26.foicore.foicoreapp.network;

import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by David on 20.5.2014..
 * <p/>
 * This interface is used for communicating with the server through given API.
 */
public interface SightsAPI {

    /**
     * Retreives array of sights from the server using GET method
     *
     * @param callback A callback which is used for processing the response
     */
    @GET("/api/test")
    void getSights(Callback<Sight[]> callback);

}
