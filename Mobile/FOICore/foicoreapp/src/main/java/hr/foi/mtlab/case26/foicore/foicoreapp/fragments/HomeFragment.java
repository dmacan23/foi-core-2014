package hr.foi.mtlab.case26.foicore.foicoreapp.fragments;

import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Random;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.FontManager;

/**
 * Created by David on 17.5.2014..
 */
@EFragment(R.layout.fragment_home)
public class HomeFragment extends SherlockFragment {

    @ViewById
    TextView txtVzFactsText;
    @ViewById
    TextView txtVzFactsTitle;

    private String[] facts;

    @AfterViews
    void main() {
        init();
    }

    private void init() {
        facts = getResources().getStringArray(R.array.vz_facts);
        int id = getPosition();
        txtVzFactsText.setText(facts[id]);
        txtVzFactsTitle.setText("Varaždin random fact");
        FontManager.setFontToTextView(txtVzFactsText, "caviar_bold.ttf");
        FontManager.setFontToTextView(txtVzFactsTitle, "caviar_bold.ttf");
    }

    private int getPosition() {
        Random rand = new Random();
        return rand.nextInt(facts.length);
        //return 6;
    }


}
