package hr.foi.mtlab.case26.foicore.foicoreapp.debug;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by David on 20.5.2014..
 */
public interface TestRestAPI {

    @GET("/api/test")
    void getTest(Callback<TestType> callback);

    @POST("/api/test")
    void postTest(@Body TestType message, Callback<TestType> callback);


}
