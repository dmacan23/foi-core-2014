package hr.foi.mtlab.case26.foicore.foicoreapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.views.AttractionItem;

/**
 * Created by David on 18.5.2014..
 */
public class AttractionsListAdapter extends BaseAdapter {

    private List<AttractionItem> items;
    private Context context;
    private int resource;

    public AttractionsListAdapter(Context context, int resource, List<AttractionItem> items) {
        this.context = context;
        this.resource = resource;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public AttractionItem getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, null);
        }

        setupView(view, i);
        return view;
    }

    private void setupView(View v, int position) {
        TextView txtLabel = (TextView) v.findViewById(R.id.txtListItemAttractionLabel);
        ImageView imgAttraction = (ImageView) v.findViewById(R.id.imgListItemAttractionImage);
        imgAttraction.setScaleType(ImageView.ScaleType.FIT_XY);

        txtLabel.setText(items.get(position).getLabel());
        Picasso.with(context).load(items.get(position).getImageURL()).placeholder(R.drawable.img_placeholder).fit().centerCrop().into(imgAttraction);
    }
}
