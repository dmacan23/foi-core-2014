package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

/**
 * Created by ahuskano on 5/26/2014.
 */
public interface ZBarConstants {
    public static final String SCAN_MODES = "SCAN_MODES";
    public static final String SCAN_RESULT = "SCAN_RESULT";
    public static final String SCAN_RESULT_TYPE = "SCAN_RESULT_TYPE";
    public static final String ERROR_INFO = "ERROR_INFO";
}
