package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;

/**
 * Created by David on 21.5.2014..
 */
public class Singleton {

    private static Singleton singleton = null;
    /**
     * A list of sights retreived from the server
     */
    private List<Sight> sights = null;
    /**
     * A single sight from the list of sights
     */
    private Sight sight;
    private GoogleMap map;
    private List<Polyline> polylines;
    private List<ImageView> images;
    private PolylineOptions polyOptions;
    private int[] gamePoints={1,2,3,4,5,6,7,8,9,10};
    private int gamePoint=1;
    private boolean isGoToChosen = false;
    private PolylineOptions polyOptionsGame;
    private List<Polyline> polylinesGame;


    private Marker markerGame;
    private MarkerOptions markerOpGame;
    private Sight sightGame;
    private boolean game=false;
    private Singleton() {
    }

    public static Singleton getInstance() {
        if (singleton == null)
            singleton = new Singleton();
        return singleton;
    }

    public boolean isGame(){
        return game;
    }
    public void setGame(boolean game){
        this.game=game;
    }
    public int getGamePoint(){
        return this.gamePoint;
    }

    public void addGamePoint(){
        this.gamePoint++;
    }
    public void setGamePoint(int i){
        this.gamePoint=i;
    }
    public int getGamePointFromArray(int i){
        return gamePoints[i];
    }
    public Sight getSightGame(){
        return sightGame;
    }

    public void setGameSight(Sight sight){
        sightGame=sight;
    }
    public void setMarkerOptions(MarkerOptions markerOptions){
        markerOpGame=markerOptions;
    }
    public MarkerOptions getMarkerOptions(){
        return markerOpGame;
    }
    public void setMarker(Marker marker){
        markerGame=marker;
    }
    public Marker getMarker(){
        return markerGame;
    }
    public boolean isGoToChosen() {
        return isGoToChosen;
    }

    public void setGoToChosen(boolean isGoToChosen) {
        this.isGoToChosen = isGoToChosen;
    }

    public void setSights(List<Sight> sights) {
        if (this.sights == null)
            this.sights = sights;
    }

    public List<Sight> getSights() {
        return this.sights;
    }

    public Sight getSight() {
        return sight;
    }

    public void setSight(Sight sight) {
        this.sight = sight;
    }

    public void setPolyline() {
        this.polylines = new ArrayList<Polyline>();
    }

    public void addPolyline(Polyline poly) {
        this.polylines.add(poly);
    }

    public List<Polyline> getPolylines() {
        return this.polylines;
    }

    public List<ImageView> getImages() {
        return this.images;
    }

    public void setImages() {
        this.images = new ArrayList<ImageView>();
    }

    public void addImage(ImageView image) {
        this.images.add(image);
    }


    public GoogleMap getMap() {
        return map;
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }

    public PolylineOptions getOptions() {
        return polyOptions;
    }

    public void setOptions(PolylineOptions polylines) {
        this.polyOptions = polylines;
    }

    public PolylineOptions getOptionsGame() {
        return polyOptionsGame;
    }

    public void setOptionsGame(PolylineOptions polylines) {
        this.polyOptionsGame= polylines;
    }

    public void addPolylineGame(Polyline poly) {
        this.polylinesGame.add(poly);
    }

    public List<Polyline> getPolylinesGame() {
        return this.polylinesGame;
    }

    public void setPolylineGame() {
        this.polylinesGame = new ArrayList<Polyline>();
    }


}
