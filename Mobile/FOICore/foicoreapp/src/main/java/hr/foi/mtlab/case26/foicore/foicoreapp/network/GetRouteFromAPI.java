package hr.foi.mtlab.case26.foicore.foicoreapp.network;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;

import hr.foi.mtlab.case26.foicore.foicoreapp.utils.DrawRoute;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.HttpConnection;

/**
 * Created by ahuskano on 5/24/2014.
 */
public class GetRouteFromAPI extends AsyncTask<String, Void, String> {
    private GoogleMap map;
    public GetRouteFromAPI(GoogleMap map){
        this.map=map;
    }
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result!=null) {
            Log.d("ruta", "getRouteFromAPI");

            new DrawRoute(map).execute(result);
        }
        }

    @Override
    protected String doInBackground(String... params) {
        HttpConnection connection = new HttpConnection();
        String data = null;
        try {
            data = connection.readUrl(params[0]);
        } catch (IOException e) {
            return null;
        }
        return data;
    }

}
