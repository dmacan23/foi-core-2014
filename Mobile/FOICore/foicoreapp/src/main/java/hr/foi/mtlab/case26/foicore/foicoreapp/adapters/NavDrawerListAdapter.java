package hr.foi.mtlab.case26.foicore.foicoreapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.views.NavDrawerItem;

/**
 * Created by David on 17.5.2014..
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private List<NavDrawerItem> items;
    private Context context;
    private int resource;

    public NavDrawerListAdapter(Context context, int resource, List<NavDrawerItem> items) {
        this.items = items;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public NavDrawerItem getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, null);
        }

        setupView(i, view);
        return view;
    }

    private void setupView(int position, View v) {
        TextView txtLabel = (TextView) v.findViewById(R.id.txtNavItemLabel);
        ImageView imgIcon = (ImageView) v.findViewById(R.id.imgNavItemIcon);

        NavDrawerItem item = items.get(position);
        txtLabel.setText(item.getLabel());
        imgIcon.setImageResource(item.getIcon());
    }
}
