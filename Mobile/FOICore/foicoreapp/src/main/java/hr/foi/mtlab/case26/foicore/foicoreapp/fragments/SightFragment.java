package hr.foi.mtlab.case26.foicore.foicoreapp.fragments;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.viewpagerindicator.CirclePageIndicator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.adapters.ImagePagerAdapter;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Const;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;

/**
 * Created by David on 24.5.2014..
 */

@EFragment(R.layout.fragment_sight)
public class SightFragment extends SherlockFragment {

    @ViewById
    ViewPager attractionCarousel;
    @ViewById
    TextView txtAttractionDetailDescription;
    @ViewById
    TextView txtAttractionDetailLabel;
    @ViewById
    CirclePageIndicator attractionIndicator;



    @AfterViews
    void main() {
        init();
    }

    private void init() {
        Sight sight = Singleton.getInstance().getSight();
        String[] images = sight.getImages();
        ImagePagerAdapter adapter = new ImagePagerAdapter(getSherlockActivity(), images);
        attractionCarousel.setAdapter(adapter);
        attractionIndicator.setViewPager(attractionCarousel);
        txtAttractionDetailDescription.setText(sight.getDescriptionShort());
        txtAttractionDetailLabel.setText(sight.getName());
        toggleNavButton();
    }


    private void toggleNavButton() {
        boolean isFromGame = getSherlockActivity().getIntent().getBooleanExtra(Const.KEY_DETAIL, true);
        if (isFromGame) {
            //           btnAttractionDetailGo.setVisibility(View.GONE);
        } else {
            //         btnAttractionDetailGo.setVisibility(View.VISIBLE);
        }
    }
}
