package hr.foi.mtlab.case26.foicore.foicoreapp;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import hr.foi.mtlab.case26.foicore.foicoreapp.debug.TestRestAPI;

/**
 * This class is used for the debugging
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends SherlockFragmentActivity {

    private static final int RESULT_SPEECH = 1;

    @ViewById
    TextView txtDebugConsole;

    TestRestAPI testRestAPI;


    private void init() {
        txtDebugConsole.setMovementMethod(new ScrollingMovementMethod());
    }

    @AfterViews
    void main() {
    }

    @Click(R.id.btnDebug1)
    void btn1Click() {
        testSTT();
    }

    @Click(R.id.btnDebug2)
    void btn2Click() {

    }

    void testSTT() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);
            txtDebugConsole.setText("");
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getApplicationContext(),
                    "Opps! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    txtDebugConsole.setText(text.get(0));
                }
                break;
            }

        }
    }

}
