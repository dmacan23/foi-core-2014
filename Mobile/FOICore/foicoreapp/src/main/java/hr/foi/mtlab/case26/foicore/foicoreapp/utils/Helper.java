package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.app.Activity;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;

/**
 * Created by ahuskano on 5/20/2014.
 */
public class Helper {

    public static float calculateDistance(LatLng locationA, LatLng locationB) {
        float[] distances = new float[1];
        Location.distanceBetween(locationA.latitude, locationA.longitude,
                locationB.latitude, locationB.longitude, distances);

        return distances[0];
    }

    public static String getMapsApiDirectionsUrl(Activity activity, LatLng locationA, LatLng locationB) {
        LatLng myLocation = getMyLocation(activity);
        String waypoints = "waypoints=optimize:true|"
                + locationA.latitude + "," + locationA.longitude
                + "|" + "|" + locationB.latitude + ","
                + locationB.longitude;
        String sensor = "sensor=false";
        String mode = "mode=walking";
        String params = waypoints + "&" + sensor + "&" + mode;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        return url;
    }

    public static LatLng getMyLocation(Activity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(activity.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);
        return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
    }

    public static void drawRoute(GoogleMap map, LatLng locationA, LatLng locationB) {
        Polyline line = map.addPolyline(new PolylineOptions()
                .add(locationA, locationB)
                .width(5)
                .color(Color.RED));
    }

    public static void setThumbImages(Activity activity) {
        Singleton sig = Singleton.getInstance();
        sig.setImages();
        for (Sight sigh : sig.getSights()) {
            ImageView image = new ImageView(activity);
            Picasso.with(activity).load(sigh.getImgThumbnail()).placeholder(R.drawable.ic_launcher).fit().centerCrop().into(image);
            sig.addImage(image);

        }
    }

    public static void drawPolylines() {
        Singleton sin = Singleton.getInstance();
        if (sin.getOptions() != null) {
            sin.addPolyline(sin.getMap().addPolyline(sin.getOptions()));
        }
    }
    public static void removePolylines(){
        Log.d("test","removePolylines");
        Singleton sin=Singleton.getInstance();
        for(Polyline poly:sin.getPolylines()){
            Log.d("android_map","remove");
            poly.remove();
        }
        sin.setOptions(null);
    }
    public static void removePolylinesGame(){
        Singleton sin=Singleton.getInstance();
        for(Polyline poly:sin.getPolylinesGame()){
            poly.remove();
        }
        sin.setOptionsGame(null);
    }
}
