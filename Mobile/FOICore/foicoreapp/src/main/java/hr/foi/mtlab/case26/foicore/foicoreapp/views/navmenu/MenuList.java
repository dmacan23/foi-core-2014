package hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.fragments.GameFragment_;
import hr.foi.mtlab.case26.foicore.foicoreapp.fragments.HomeFragment_;
import hr.foi.mtlab.case26.foicore.foicoreapp.fragments.MapFragment;
import hr.foi.mtlab.case26.foicore.foicoreapp.fragments.PlacesFragment_;

/**
 * Created by David on 26.5.2014..
 */
public class MenuList extends SherlockListFragment {

    public static int currentItem = 0;

    public static final NavMenuItem[] menuItems = {
            new NavMenuItem(R.drawable.ic_icon_home, "Home", new HomeFragment_()),
            new NavMenuItem(R.drawable.ic_icon_map, "Map", new MapFragment()),
            new NavMenuItem(R.drawable.ic_ic_icon_places, "Sights", new PlacesFragment_()),
            new NavMenuItem(R.drawable.ic_icon_game, "Game", new GameFragment_())
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_list, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListAdapter(new MenuListAdapter(getSherlockActivity(), menuItems));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        android.support.v4.app.FragmentManager fm = getSherlockActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_frame, show(position));
        ft.commit();

        setupSlidingMenuParams(position);

        currentItem = position;
    }

    private SherlockFragment show(int position) {
        return menuItems[position].getFragment();
    }

    private void setupSlidingMenuParams(int position) {
        if (BaseActivity.TOUCHMODE_DEFAULT == SlidingMenu.TOUCHMODE_FULLSCREEN) {
            switch (position) {
                case 1:
                    ((BaseActivity) this.getSherlockActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
                    break;
                default:
                    ((BaseActivity) this.getSherlockActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
                    break;
            }
        }

        if (BaseActivity.TOUCHMODE_DEFAULT == SlidingMenu.TOUCHMODE_FULLSCREEN)
            ((BaseActivity) this.getSherlockActivity()).getSlidingMenu().toggle();
    }

}
