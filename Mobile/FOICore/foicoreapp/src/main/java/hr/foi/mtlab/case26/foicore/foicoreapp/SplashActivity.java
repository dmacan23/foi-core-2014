package hr.foi.mtlab.case26.foicore.foicoreapp;

import android.content.Intent;
import android.os.Handler;

import com.actionbarsherlock.app.SherlockActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Const;
import hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu.BaseActivity;

/**
 * Created by David on 17.5.2014..
 */
@EActivity(R.layout.activity_splash)
public class SplashActivity extends SherlockActivity {

    @AfterViews
    void main() {
        splash();
    }

    private void splash() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = null;
                // i = new Intent(getBaseContext(), NavigationActivity_.class);
                i = new Intent(getBaseContext(), BaseActivity.class);
                startActivity(i);
                finish();
            }
        }, Const.SPLASH_TIMEOUT);
    }

}
