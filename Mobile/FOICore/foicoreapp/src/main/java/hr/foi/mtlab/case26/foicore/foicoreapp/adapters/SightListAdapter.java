package hr.foi.mtlab.case26.foicore.foicoreapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Animator;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.FontManager;

/**
 * Created by David on 20.5.2014..
 */
public class SightListAdapter extends BaseAdapter {

    private List<Sight> items;
    private Context context;
    private int resource;

    public SightListAdapter(Context context, int resource, List<Sight> items) {
        this.items = items;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Sight getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, null);
        }

        setupView(view, i);
        return view;
    }

    private void setupView(View v, int position) {
        Animator.animateListItem(v, context, position);

        TextView txtLabel = (TextView) v.findViewById(R.id.txtListItemAttractionLabel);
        ImageView imgAttraction = (ImageView) v.findViewById(R.id.imgListItemAttractionImage);
        imgAttraction.setScaleType(ImageView.ScaleType.FIT_XY);
        txtLabel.setText(items.get(position).getName().toUpperCase(Locale.getDefault()));
        FontManager.setFontToTextView(txtLabel, "caviar_bold.ttf");
        Picasso.with(context).load(items.get(position).getImg1()).placeholder(R.drawable.img_placeholder).fit().centerCrop().into(imgAttraction);
    }
}
