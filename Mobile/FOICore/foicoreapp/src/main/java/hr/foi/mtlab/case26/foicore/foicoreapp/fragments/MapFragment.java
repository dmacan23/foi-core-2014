package hr.foi.mtlab.case26.foicore.foicoreapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.Arrays;
import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.AttractionDetailActivity_;
import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.network.GetRouteFromAPI;
import hr.foi.mtlab.case26.foicore.foicoreapp.network.SightsAPI;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Const;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Helper;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;
import hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu.BaseActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 17.5.2014..
 */
//@EFragment(R.layout.fragment_map)
public class MapFragment extends SherlockFragment {
    static View view;
    private GoogleMap map;
    private SightsAPI sightsAPI;
    private ProgressDialog dialog;
    /**
     * A callback which is called immediately after the HTTP request receives response
     */
    private Callback<Sight[]> callback = new Callback<Sight[]>() {
        @Override
        public void success(Sight[] sight, Response response) {
            showSights(sight);
            closeDialog();
        }

        @Override
        public void failure(RetrofitError error) {
            closeDialog();
            toast("An error has ocurred!");
        }
    };


    /*
    @AfterViews
    void main() {
        map = ((SupportMapFragment) getSherlockActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (Singleton.getInstance().getSights() != null) {
            setMap();
            if (Singleton.getInstance().isGoToChosen()) {
                showSightRoute();
            }
        } else {
            getSights();
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        map = ((SupportMapFragment) getSherlockActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (Singleton.getInstance().getSights() != null) {
            setMap();
            if (Singleton.getInstance().isGoToChosen()) {
                showSightRoute();
            }
        } else {
            getSights();
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    private void setMap() {

        addMarkers();

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                List<Sight> sights = Singleton.getInstance().getSights();
                LayoutInflater inflater = (LayoutInflater) getSherlockActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.marker_info, null);
                TextView snipet = (TextView) v.findViewById(R.id.snipet);
                snipet.setText(sights.get(Integer.valueOf(marker.getSnippet()) - 1).getName());
                TextView distance = (TextView) v.findViewById(R.id.distance);
                distance.setText(Helper.calculateDistance(Helper.getMyLocation(getActivity()), marker.getPosition()) + "m");
                return v;
            }
        });
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });
        map.setMyLocationEnabled(true);
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
                Singleton.getInstance().setSight(Singleton.getInstance().getSights().get(Integer.valueOf(marker.getSnippet()) - 1));
                Intent i = new Intent(getSherlockActivity(), AttractionDetailActivity_.class);
                i.putExtra(Const.KEY_DETAIL, false);
                startActivity(i);
            }
        });
        Singleton.getInstance().setMap(map);
        // Helper.removePolylines();
        //Helper.drawPolylines();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getSherlockActivity()).getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        map = ((SupportMapFragment) getSherlockActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (Singleton.getInstance().getSights() != null) {
            // setMap();
        }
        if (Singleton.getInstance().isGoToChosen()) {

            //  showSightRoute();
        }
    }

    private void addMarkers() {
        List<Sight> sights = Singleton.getInstance().getSights();
      Bitmap icon = BitmapFactory.decodeResource(getSherlockActivity().getApplicationContext().getResources(),
              R.drawable.map_pin);
        BitmapDescriptor descriptor = BitmapDescriptorFactory.fromBitmap(icon);
        for (Sight sigh : sights) {

            map.addMarker(new MarkerOptions().position(new LatLng(sigh.getLattitude(), sigh.getLongitude())).snippet("" + sigh.getId()).icon(descriptor));
        }
    }

    private void showSightRoute() {
        //Odabrani sight se nalazi u Singletonu: Singleton.getInstance().getSight();
        Helper.removePolylines();
        GetRouteFromAPI route = new GetRouteFromAPI(Singleton.getInstance().getMap());
        route.execute(Helper.getMapsApiDirectionsUrl(getSherlockActivity(), Helper.getMyLocation(getSherlockActivity()), new LatLng(Singleton.getInstance().getSight().getLattitude(), Singleton.getInstance().getSight().getLongitude())));

        // Ne briši
        Singleton.getInstance().setGoToChosen(false);
    }

    /**
     * Retreives sights from the server and registers the callback for response
     */
    private void getSights() {
        showDialog();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer(Const.SERVER_LOCAL)
                .build();
        sightsAPI = restAdapter.create(SightsAPI.class);
        sightsAPI.getSights(callback);
    }

    /**
     * Displays the list of sights inside the ListView
     *
     * @param sights An array of sights to be displayed
     */
    private void showSights(Sight[] sights) {
        Singleton.getInstance().setSights(Arrays.asList(sights));
        setMap();
    }

    /**
     * Helping methods
     */
    private void showDialog() {
        dialog = ProgressDialog.show(getSherlockActivity(), "", "Loading...", true);
    }

    private void closeDialog() {
        dialog.dismiss();
    }

    private void toast(String message) {
        Toast.makeText(getSherlockActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
