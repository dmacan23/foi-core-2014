package hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;

/**
 * Created by David on 26.5.2014..
 */
public class MenuListAdapter extends BaseAdapter {

    private Context context;
    private NavMenuItem[] menuItems;

    public MenuListAdapter(Context context, NavMenuItem[] menuItems) {
        this.context = context;
        this.menuItems = menuItems;
    }

    @Override
    public int getCount() {
        return menuItems.length;
    }

    @Override
    public NavMenuItem getItem(int position) {
        return menuItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.nav_list_item, null);
        }

        setupView(position, convertView);
        return convertView;
    }

    private void setupView(int position, View v) {
        TextView txtLabel = (TextView) v.findViewById(R.id.txtNavItemLabel);
        ImageView imgIcon = (ImageView) v.findViewById(R.id.imgNavItemIcon);

        NavMenuItem item = menuItems[position];

        txtLabel.setText(item.getLabel());
        imgIcon.setImageResource(item.getIconRes());
    }
}
