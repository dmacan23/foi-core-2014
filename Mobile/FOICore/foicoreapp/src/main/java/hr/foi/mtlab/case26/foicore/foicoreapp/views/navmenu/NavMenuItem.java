package hr.foi.mtlab.case26.foicore.foicoreapp.views.navmenu;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * Created by David on 26.5.2014..
 */
public class NavMenuItem {

    private int iconRes;
    private String label;
    private SherlockFragment fragment;

    public NavMenuItem(int iconRes, String label, SherlockFragment fragment) {
        this.iconRes = iconRes;
        this.label = label;
        this.fragment = fragment;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public SherlockFragment getFragment() {
        return fragment;
    }

    public void setFragment(SherlockFragment fragment) {
        this.fragment = fragment;
    }
}
