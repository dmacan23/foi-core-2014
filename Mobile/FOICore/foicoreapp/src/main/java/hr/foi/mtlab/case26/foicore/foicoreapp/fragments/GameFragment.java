package hr.foi.mtlab.case26.foicore.foicoreapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

import hr.foi.mtlab.case26.foicore.foicoreapp.GameActivity_;
import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.network.SightsAPI;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Const;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.FontManager;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 17.5.2014..
 */
@EFragment(R.layout.fragment_game)
public class GameFragment extends SherlockFragment {
    @ViewById
    Button btGame;
    @ViewById
    TextView txtGameDescription;

    private SightsAPI sightsAPI;
    private ProgressDialog dialog;


    @AfterViews
    void main() {
        FontManager.setFontToTextView(txtGameDescription, "caviar_bold.ttf");
    }

    @Click(R.id.btGame)
    void startGame() {
        if (Singleton.getInstance().getSights() == null)
            getSights();
        else
            startActivityForResult(new Intent(getSherlockActivity(), GameActivity_.class), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

        }
    }

    /**
     * Retreives sights from the server and registers the callback for response
     */
    private void getSights() {
        showDialog();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer(Const.SERVER_LOCAL)
                .build();
        sightsAPI = restAdapter.create(SightsAPI.class);
        sightsAPI.getSights(callback);
    }

    /**
     * Displays the list of sights inside the ListView
     *
     * @param sights An array of sights to be displayed
     */
    private void showSights(Sight[] sights) {
        Singleton.getInstance().setSights(Arrays.asList(sights));
        startActivityForResult(new Intent(getSherlockActivity(), GameActivity_.class), 1);
    }

    /**
     * A callback which is called immediately after the HTTP request receives response
     */
    private Callback<Sight[]> callback = new Callback<Sight[]>() {
        @Override
        public void success(Sight[] sight, Response response) {
            showSights(sight);
            closeDialog();
        }

        @Override
        public void failure(RetrofitError error) {
            closeDialog();
            toast("An error has ocurred!");
        }
    };


    /**
     * Helping methods
     */
    private void showDialog() {
        dialog = ProgressDialog.show(getSherlockActivity(), "", "Loading...", true);
    }

    private void closeDialog() {
        dialog.dismiss();
    }

    private void toast(String message) {
        Toast.makeText(getSherlockActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
