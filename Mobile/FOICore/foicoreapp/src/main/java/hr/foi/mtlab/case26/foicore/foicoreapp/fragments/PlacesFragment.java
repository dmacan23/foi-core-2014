package hr.foi.mtlab.case26.foicore.foicoreapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

import hr.foi.mtlab.case26.foicore.foicoreapp.AttractionDetailActivity_;
import hr.foi.mtlab.case26.foicore.foicoreapp.R;
import hr.foi.mtlab.case26.foicore.foicoreapp.adapters.SightListAdapter;
import hr.foi.mtlab.case26.foicore.foicoreapp.network.SightsAPI;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Const;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 17.5.2014..
 * <p/>
 * Fragment class used to display the list of sights in the application.
 * This fragment is part of the main navigation activity.
 */
@EFragment(R.layout.fragment_places)
public class PlacesFragment extends SherlockFragment {

    @ViewById
    ListView listAttractions;

    private SightListAdapter adapter;
    private SightsAPI sightsAPI;
    private ProgressDialog dialog;

    @AfterViews
    void main() {
        // REAL WAY:
        //Singleton.getInstance().setSights(DataGenerator.generateSights());
        if (Singleton.getInstance().getSights() == null)
            getSights();
        else
            displayList();
    }

    private void displayList() {
        adapter = new SightListAdapter(getSherlockActivity(), R.layout.list_item_attraction, Singleton.getInstance().getSights());
        listAttractions.setAdapter(adapter);
    }

    @ItemClick
    void listAttractionsItemClicked(Sight sight) {
        startActivity(sight);
    }

    private void startActivity(Sight sight) {
        Singleton.getInstance().setSight(sight);
        Intent i = new Intent(getSherlockActivity(), AttractionDetailActivity_.class);
        i.putExtra(Const.KEY_DETAIL, false);
        startActivity(i);
    }

    /**
     * Retreives sights from the server and registers the callback for response
     */
    private void getSights() {
        showDialog();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer(Const.SERVER_LOCAL)
                .build();
        sightsAPI = restAdapter.create(SightsAPI.class);
        sightsAPI.getSights(callback);
    }

    /**
     * Displays the list of sights inside the ListView
     *
     * @param sights An array of sights to be displayed
     */
    private void showSights(Sight[] sights) {
        Singleton.getInstance().setSights(Arrays.asList(sights));
        displayList();
    }

    /**
     * A callback which is called immediately after the HTTP request receives response
     */
    private Callback<Sight[]> callback = new Callback<Sight[]>() {
        @Override
        public void success(Sight[] sight, Response response) {
            showSights(sight);
            closeDialog();
        }

        @Override
        public void failure(RetrofitError error) {
            closeDialog();
            toast("An error has ocurred!");
        }
    };


    /**
     * Helping methods
     */
    private void showDialog() {
        dialog = ProgressDialog.show(getSherlockActivity(), "", "Loading...", true);
    }

    private void closeDialog() {
        dialog.dismiss();
    }

    private void toast(String message) {
        Toast.makeText(getSherlockActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
