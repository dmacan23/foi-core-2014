package hr.foi.mtlab.case26.foicore.foicoreapp.views;

/**
 * Created by David on 17.5.2014..
 */
public class NavDrawerItem {

    private String label;
    private int icon;

    public NavDrawerItem() {

    }

    public NavDrawerItem(String label, int icon) {
        this.label = label;
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
