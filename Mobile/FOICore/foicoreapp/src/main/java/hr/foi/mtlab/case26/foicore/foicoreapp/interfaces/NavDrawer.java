package hr.foi.mtlab.case26.foicore.foicoreapp.interfaces;

/**
 * Created by David on 17.5.2014..
 * An interface used for easing the navigation drawer implementation
 */
public interface NavDrawer {

    /**
     * This method initializes items for the list inside the navigation drawer
     */
    void initDrawerItems();

    /**
     * This method initializes appropriate fragments for the navigation drawer
     */
    void initFragments();

    /**
     * This method is used for setting up the navigation drawer, initializing its adapter etc.
     */
    void setupDrawer();

    /**
     * This method is used to configure the navigation drawer toggle
     */
    void configureDrawerToggle();

    /**
     * This method displays the fragment at the given position
     *
     * @param position The position of the fragment that is to be displayed
     */
    boolean displayView(int position);

}
