package hr.foi.mtlab.case26.foicore.foicoreapp.debug;

import java.util.ArrayList;
import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.views.AttractionItem;

/**
 * Created by David on 18.5.2014..
 */
public class DataGenerator {

    public static List<AttractionItem> generateAttractions() {
        List<AttractionItem> items = new ArrayList<AttractionItem>();
        items.add(new AttractionItem("Grgur ninski", "Opis Grgura ninskog", "http://upload.wikimedia.org/wikipedia/commons/1/18/Split-GrgurNiski-IvanMestrovic.jpg"));
        items.add(new AttractionItem("Korzo", "Čista zajebancija i čiliranje", "http://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Clocher-varazdin.JPG/800px-Clocher-varazdin.JPG"));
        items.add(new AttractionItem("FOI", "Smeće", "http://www.evarazdin.hr/cms/wp-content/uploads/2010/09/FOI_Studenti.jpg"));
        items.add(new AttractionItem("Stari grad", "Super mjesto", "http://varazdinairport.com/content/images/dvorci/stari_dvor_varazdin.jpg"));
        items.add(new AttractionItem("Studentski dom Varaždin", "Ludnica!", "http://croatia.hr/Images/t900x600-10492/hostel.jpg"));
        items.add(new AttractionItem("Grgur ninski", "Opis Grgura ninskog", "http://upload.wikimedia.org/wikipedia/commons/1/18/Split-GrgurNiski-IvanMestrovic.jpg"));
        items.add(new AttractionItem("Korzo", "Čista zajebancija i čiliranje", "http://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Clocher-varazdin.JPG/800px-Clocher-varazdin.JPG"));
        items.add(new AttractionItem("FOI", "Smeće", "http://www.evarazdin.hr/cms/wp-content/uploads/2010/09/FOI_Studenti.jpg"));
        items.add(new AttractionItem("Stari grad", "Super mjesto", "http://varazdinairport.com/content/images/dvorci/stari_dvor_varazdin.jpg"));
        items.add(new AttractionItem("Studentski dom Varaždin", "Ludnica!", "http://croatia.hr/Images/t900x600-10492/hostel.jpg"));
        items.add(new AttractionItem("Stari grad", "Super mjesto", "http://varazdinairport.com/content/images/dvorci/stari_dvor_varazdin.jpg"));
        items.add(new AttractionItem("Studentski dom Varaždin", "Ludnica!", "http://croatia.hr/Images/t900x600-10492/hostel.jpg"));
        items.add(new AttractionItem("Grgur ninski", "Opis Grgura ninskog", "http://upload.wikimedia.org/wikipedia/commons/1/18/Split-GrgurNiski-IvanMestrovic.jpg"));
        items.add(new AttractionItem("Grgur ninski", "Opis Grgura ninskog", "http://upload.wikimedia.org/wikipedia/commons/1/18/Split-GrgurNiski-IvanMestrovic.jpg"));
        items.add(new AttractionItem("Korzo", "Čista zajebancija i čiliranje", "http://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Clocher-varazdin.JPG/800px-Clocher-varazdin.JPG"));
        items.add(new AttractionItem("FOI", "Smeće", "http://www.evarazdin.hr/cms/wp-content/uploads/2010/09/FOI_Studenti.jpg"));
        items.add(new AttractionItem("Stari grad", "Super mjesto", "http://varazdinairport.com/content/images/dvorci/stari_dvor_varazdin.jpg"));
        return items;
    }

    public static String[] images = {"http://varazdinairport.com/content/images/dvorci/stari_dvor_varazdin.jpg", "http://www.varazdin.hr/cms-repository/image/limit_800_600/291.jpg", "http://upload.wikimedia.org/wikipedia/commons/0/02/Stari_grad_Vara%C5%BEdin.JPG"};

    public static String personJSON = "{\n" +
            "  name:\"Albert Attard\",\n" +
            "  P_LANGUAGE:\"Java\",\n" +
            "  location:\"Malta\"\n" +
            "}";

    public static String testResponseApi = "https://api.twitter.com";
    public static String testPathPOST = "/oauth2/token";
    public static String testPathBody = "grant_type=client_credentials";


    public static String lorem = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.";

    public static List<Sight> generateSights() {
        List<Sight> sights = new ArrayList<Sight>();

        sights.add(new Sight(1, 46.307937, 16.33756, "http://www.varazdin-online.com/grad/znamenitosti/palace/ritz/107_0773.jpg", "http://www.tourism-varazdin.hr/media/k2/items/cache/36fdb1a35cd2f54f95cf2119fb5bc7ed_L.jpg", "http://inlovewithtraveling.com/wp-content/uploads/2014/01/VARAZDIN_kuca_Ritz.jpg", "http://www.lupiga.com/repository/vijesti/slike/20120403063106varazdin_kuca_ritz_franjevacka_crkva2.jpg", "The house Ritz", "This one-story angle house with a Renaissance arcature on the first floor, is one of the oldest houses in Varaždin. The stone lintel, beneath the porch with carved LA. monogram and the year 1540, has been preserved.", lorem, "eng"));
        sights.add(new Sight(2, 46.307832, 16.337947, "http://old.foi.hr/CMS_home/upisi/foi.jpg", "http://www.evarazdin.hr/cms/wp-content/uploads/2011/03/FOI_Varazdin_Studenti.jpg", "http://www.varazdin-info.com/wp-content/uploads/2012/12/FOI1.jpg", "http://www.evarazdin.hr/cms/wp-content/uploads/2013/11/FOI-Foto-Andrej-%C5%A0voger.jpg", "The Jesuit Monastery", "A former Jesuit monastery erected at the end of the 17th century, later became a Paulin monastery. Nowdays, the monastery with the church, high school and seminary is the most valuable complex of the early Baroque arhitecture of Varaždin.", lorem, "eng"));
        sights.add(new Sight(3, 46.307779, 16.336338, "http://varazdinguide.com/hrv/wp-content/uploads/2011/02/IMG_5464-150x150.jpg", "http://4.bp.blogspot.com/_17rXtcI037Q/TIpvA90QBiI/AAAAAAAAALU/8VKLVdN0w98/s640/DSC00051.JPG", "http://www.tourism-club.com/multimedia/images/small/admin_P3091663.JPG", "http://www.celestialscenes.com/sundials/img/varazdin-franjo-2-20081008-skokic.jpg", "The Franciscan church of st. John the Baptist", "The church was built in 1650 on the site of a medieval church of Knight Hospitaliers Peter Rabba fromGraz, was its constructor. The tower, erected in 1641, is the tallest in Varaždin (54,5m)", lorem, "eng"));
        sights.add(new Sight(4, 46.309489, 16.336317, "http://hvm.mdc.hr/UserFiles/Image/GM_Varazdin/4-Sermage-05890009.jpg", "http://regionalni.com/upload/publish/11654/thumb/palacasermage_5077c9a9a82a3_650x360c.jpg", "http://www.radio-varazdin.hr/cms/upload/slike/2660221511022412_sermage%20(500%20x%20375).jpg", "http://www.lero.com.hr/EasyEdit/UserFiles/Blog/nastavak-lero-trg-avanture-varazdin/nastavak-lero-trg-avanture-varazdin-3_310_232.jpeg", "The Sermage Palace", "The harmonious Rococo palace takes the today appearance in mid 18th century. Its owner in the 17st century was baron Prassinzky and later the Sermage family.", lorem, "eng"));
        sights.add(new Sight(5, 46.309929, 16.33445, "http://www.insula-tours.hr/insula/wp-content/uploads/2009/12/varazdin_stari_grad.jpg", "http://www.idemvan.hr/content_images/header449972742wsthpbkmgn.jpg", "http://www.idemvan.hr/content_images/header449972742wsthpbkmgn.jpg", "http://www.idemvan.hr/content_images/header449972742wsthpbkmgn.jpg", "The Old Town", "The construction of the fortress lasted from the end of the 13th century until mid 19th century. It was the main seat of Varaždin feudal families for hundreds of years. In the 16th century it gets high earthen walls and double water-filled ditches.", lorem, "eng"));
        sights.add(new Sight(6, 46.30824, 16.335293, "http://www.kerameikon.com/varazdin/images/img09z/00003aursulinski%20samostan.jpg", "http://www.evarazdin.hr/cms/wp-content/uploads/2012/10/Ur%C5%A1ulinska-crkva-485x273.jpg", "http://www.evarazdin.hr/cms/wp-content/uploads/2012/10/Ur%C5%A1ulinska-crkva-485x273.jpg", "http://www.evarazdin.hr/cms/wp-content/uploads/2012/10/Ur%C5%A1ulinska-crkva-485x273.jpg", "The Ursuline convent", "The Ursuline sisters bought this house and the church grounds in the mid-18th century to build a convent and a girls school, marking the beginning of their long educational tradition.", lorem, "eng"));

        return sights;
    }
}
