package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ahuskano on 5/24/2014.
 */
public class DrawRoute extends
        AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
    private GoogleMap map;

    public DrawRoute(GoogleMap map) {
        this.map = map;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(
            String... jsonData) {
        if (jsonData != null) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return routes;
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
        ArrayList<LatLng> points = null;
        PolylineOptions polyLineOptions = null;
        if (routes == null) {
            return;
        }
        // traversing through routes
        for (int i = 0; i < routes.size(); i++) {
            points = new ArrayList<LatLng>();
            polyLineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = routes.get(i);

            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            polyLineOptions.addAll(points);
            polyLineOptions.width(5);
            polyLineOptions.color(Color.RED);
        }
        Log.d("ruta", "drawRoute");
        if (!Singleton.getInstance().isGame()) {

            Log.d("ruta", "drawRoute 1");
            Singleton.getInstance().addPolyline(map.addPolyline(polyLineOptions));
            Singleton.getInstance().setOptions(polyLineOptions);
        } else {

            Log.d("ruta", "drawRoute 2");
            Singleton.getInstance().addPolylineGame(map.addPolyline(polyLineOptions));
            Singleton.getInstance().setOptionsGame(polyLineOptions);
            Helper.drawPolylines();
        }
    }
}
