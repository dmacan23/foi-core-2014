package hr.foi.mtlab.case26.foicore.foicoreapp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import hr.foi.mtlab.case26.foicore.foicoreapp.network.GetRouteFromAPI;
import hr.foi.mtlab.case26.foicore.foicoreapp.types.Sight;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Helper;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.Singleton;
import hr.foi.mtlab.case26.foicore.foicoreapp.utils.ZBarConstants;

/**
 * Created by ahuskano on 5/26/2014.
 */
@EActivity(R.layout.activity_game)
public class GameActivity extends SherlockFragmentActivity {

    @ViewById
    ImageButton btQR;

    GoogleMap mapGame;

    @AfterViews
    void main() {
        final String[] zagonetke = getResources().getStringArray(R.array.riddles);
        Singleton.getInstance().setGamePoint(0);
        Singleton.getInstance().setPolylineGame();
        Singleton.getInstance().setGame(true);
        mapGame = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapGame)).getMap();
        mapGame.clear();
        Sight sigh = Singleton.getInstance().getSights().get(0);
        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(sigh.getLattitude(), sigh.getLongitude())).snippet("" + sigh.getId());
        Marker marker = mapGame.addMarker(markerOptions);
        Singleton.getInstance().setGameSight(sigh);
        //      Singleton.getInstance().setMarker(marker);
        //     Singleton.getInstance().setMarkerOptions(markerOptions);
        showSightRoute();
        mapGame.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                List<Sight> sights = Singleton.getInstance().getSights();
                LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.marker_info, null);
                TextView snipet = (TextView) v.findViewById(R.id.snipet);
                snipet.setText(zagonetke[Singleton.getInstance().getGamePoint()]);
                TextView distance = (TextView) v.findViewById(R.id.distance);
                distance.setText(Helper.calculateDistance(Helper.getMyLocation(GameActivity.this), marker.getPosition()) + "m");
                return v;
            }
        });
        mapGame.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });
        mapGame.setMyLocationEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("sad", "onResume");
        if (Singleton.getInstance().getMarkerOptions() != null) {
            mapGame.addMarker(Singleton.getInstance().getMarkerOptions());
            showSightRoute();
        }
    }

    @Click(R.id.btQR)
    void startScan() {
        startActivityForResult(new Intent(GameActivity.this, ZBarScannerActivityOrder.class), 11);
    }

    private void removeMarkers() {
        mapGame.clear();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            int number = Integer.valueOf(data.getStringExtra(ZBarConstants.SCAN_RESULT));
            if (number > 0 && number < 11) {
                try {
                    if (number - 1 == Singleton.getInstance().getGamePoint()) {
                        Singleton.getInstance().setGame(true);
                        Singleton.getInstance().addGamePoint();
                        Helper.removePolylinesGame();
                        removeMarkers();
                        Sight sigh = Singleton.getInstance().getSights().get(number - 1);
                        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(sigh.getLattitude(), sigh.getLongitude())).snippet("" + sigh.getId());
                        Marker marker = mapGame.addMarker(markerOptions);
                        Singleton.getInstance().setGameSight(sigh);
                        Singleton.getInstance().setMarker(marker);
                        Singleton.getInstance().setMarkerOptions(markerOptions);
                        showSightRoute();
                        Singleton.getInstance().setSight(Singleton.getInstance().getSights().get(Integer.valueOf(marker.getSnippet()) - 1));
                        startActivity(new Intent(GameActivity.this, AttractionDetailActivity_.class));

                    } else {
                        Toast.makeText(this,  "That's not right! Find another one :)", Toast.LENGTH_SHORT)
                                .show();
                    }

                } catch (Exception e) {

                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Camera unavailable", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("sad", "onDestroy");
    }

    private void showSightRoute() {
        GetRouteFromAPI route = new GetRouteFromAPI(mapGame);
        route.execute(Helper.getMapsApiDirectionsUrl(this, Helper.getMyLocation(this), new LatLng(Singleton.getInstance().getSightGame().getLattitude(), Singleton.getInstance().getSightGame().getLongitude())));
    }

}
