package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

/**
 * Created by David on 17.5.2014..
 */
public class Const {

    public static final int SPLASH_TIMEOUT = 1000;

    public static final String KEY_NAME = "name";
    public static final String KEY_DESC_SHORT = "desc_short";
    public static final String KEY_DESC_LONG = "desc_long";
    public static final String KEY_IMG1 = "img1";
    public static final String KEY_IMG2 = "img2";
    public static final String KEY_IMG3 = "img3";

    public static final String KEY_DETAIL = "details";

    //public static final String SERVER_LOCAL = "http://10.11.244.115";
    //public static final String SERVER_LOCAL = "http://192.168.158.52";
    public static final String SERVER_LOCAL = "http://denispav-001-site1.myasp.net/";

}
