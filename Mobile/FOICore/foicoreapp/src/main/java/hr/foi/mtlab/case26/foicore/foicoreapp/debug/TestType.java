package hr.foi.mtlab.case26.foicore.foicoreapp.debug;

import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 20.5.2014..
 */
public class TestType {

    @SerializedName("message")
    private String[] message;
    @SerializedName("status")
    private String status;

    public TestType() {

    }

    public TestType(String[] message, String status) {
        this.message = message;
        this.status = status;
    }

    public String[] getMessage() {
        return message;
    }

    public void setMessage(String[] message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
