package hr.foi.mtlab.case26.foicore.foicoreapp;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by David on 18.5.2014..
 */
@EActivity(R.layout.activity_attraction_details)
public class AttractionDetailActivity extends SherlockFragmentActivity {
    /*
        @ViewById
        ViewPager attractionCarousel;
        @ViewById
        TextView txtAttractionDetailDescription;
        @ViewById
        TextView txtAttractionDetailLabel;
        @ViewById
        CirclePageIndicator attractionIndicator;

        */
    @AfterViews
    void main() {
        //  txtAttractionDetailDescription.setMovementMethod(new ScrollingMovementMethod());
        //init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
/*
    private void init() {
        Intent i = getIntent();
        String[] images = {i.getStringExtra(Const.KEY_IMG1), i.getStringExtra(Const.KEY_IMG2), i.getStringExtra(Const.KEY_IMG3)};
        ImagePagerAdapter adapter = new ImagePagerAdapter(getBaseContext(), images);
        attractionCarousel.setAdapter(adapter);
        attractionIndicator.setViewPager(attractionCarousel);
        txtAttractionDetailDescription.setText(i.getStringExtra(Const.KEY_DESC_LONG));
        txtAttractionDetailLabel.setText(i.getStringExtra(Const.KEY_NAME));
    }*/
}
