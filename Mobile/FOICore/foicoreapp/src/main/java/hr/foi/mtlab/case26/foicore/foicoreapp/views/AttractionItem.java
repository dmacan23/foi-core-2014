package hr.foi.mtlab.case26.foicore.foicoreapp.views;

/**
 * Created by David on 18.5.2014..
 */
public class AttractionItem {

    private String label;
    private String description;
    private String imageURL;

    public AttractionItem() {

    }

    public AttractionItem(String label, String description, String imageURL) {
        this.label = label;
        this.description = description;
        this.imageURL = imageURL;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
