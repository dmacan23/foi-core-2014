package hr.foi.mtlab.case26.foicore.foicoreapp.types;

import android.app.Activity;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;

/**
 * Created by David on 20.5.2014..
 */
public class Sight {
    @SerializedName("id")
    private int id;
    @SerializedName("lattitude")
    private double lattitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("img_thumb")
    private String imgThumbnail;
    @SerializedName("img_1")
    private String img1;
    @SerializedName("img_2")
    private String img2;
    @SerializedName("img_3")
    private String img3;
    @SerializedName("name")
    private String name;
    @SerializedName("desc_short")
    private String descriptionShort;
    @SerializedName("desc_long")
    private String descriptionLong;
    @SerializedName("language")
    private String language;

    public Sight() {
    }

    public Sight(int id, double lattitude, double longitude, String imgThumbnail, String img1, String img2, String img3, String name, String descriptionShort, String descriptionLong, String language) {
        this.id = id;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.imgThumbnail = imgThumbnail;
        this.img1 = img1;
        this.img2 = img2;
        this.img3 = img3;
        this.name = name;
        this.descriptionShort = descriptionShort;
        this.descriptionLong = descriptionLong;
        this.language = language;

    }
    public String[] getImages() {
        return new String[]{this.img1, this.img2, this.img3};
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getImgThumbnail() {
        return imgThumbnail;
    }

    public void setImgThumbnail(String imgThumbnail) {
        this.imgThumbnail = imgThumbnail;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    }
