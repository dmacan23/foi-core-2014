package hr.foi.mtlab.case26.foicore.foicoreapp.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;

/**
 * Created by David on 18.5.2014..
 */
public class ImagePagerAdapter extends PagerAdapter {

    private String[] images;
    private Context context;

    public ImagePagerAdapter(Context context, String[] images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView img = new ImageView(context);
        img.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        Picasso.with(context).load(images[position]).placeholder(R.drawable.img_placeholder).fit().centerCrop().into(img);

        container.addView(img);
        return img;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
