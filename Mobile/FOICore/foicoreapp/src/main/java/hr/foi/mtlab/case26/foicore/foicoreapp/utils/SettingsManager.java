package hr.foi.mtlab.case26.foicore.foicoreapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import hr.foi.mtlab.case26.foicore.foicoreapp.R;

/**
 * Created by David on 26.5.2014..
 */
public class SettingsManager {

    public static String getDisplayLanguage(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String[] languages = context.getResources().getStringArray(R.array.languages);
        return sharedPrefs.getString("languagePref", languages[0]);
    }

}
